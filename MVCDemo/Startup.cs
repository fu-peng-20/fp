using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MVCDemo.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVCDemo
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        // 为程序注入服务
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();//新增该行代码，为程序注入MVC服务

            //为从程序添加数据上下文
            //services.AddDbContext<McStoreContext>();
            services.AddDbContext<Store_2022Context>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //使用静态文件
            app.UseStaticFiles();

            app.UseRouting();

            //常规路由 全局生效
            app.UseEndpoints(endpoints =>
            {
                //endpoints.MapGet("/", async context =>
                //{
                //    await context.Response.WriteAsync("Hello World!");
                //});
                //配置路由规则
                endpoints.MapControllerRoute(
                   //规则的名字
                  name: "default",
                  //规则的配置 控制器=Home(默认控制器)/方法=Index(默认方法)/参数？（可选）
                  pattern: "{controller=Home}/{action=Index}/{id?}");

                endpoints.MapControllerRoute(
                 //规则的名字
                 name: "ldc",
                 //规则的配置 控制器=Home(默认控制器)/方法=Index(默认方法)/参数？（可选）
                 pattern: "ldc/{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
