﻿using Microsoft.AspNetCore.Mvc;
using MVCDemo.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVCDemo.Controllers
{
    /// <summary>
    /// Home控制器
    /// </summary>
    public class HomeController : Controller
    {

        private readonly Store_2022Context _db;
        public HomeController(Store_2022Context db)
        {
            _db = db;
        }


        public IActionResult Index()
        {
            //找到页面
            return View();
        }
    }
}
