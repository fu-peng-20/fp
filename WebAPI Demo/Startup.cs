using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using WebAPI_Demo.Controllers;
using WebAPI_Demo.Filter;
using WebAPI_Demo.Models;
using WebAPI_Demo.Models.Database;
using WebAPI_Demo.Service;
using WebAPI_Demo.Services;

namespace WebAPI_Demo
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //注入控制器服务
            services.AddControllers(x =>
            {
                //会对于所有控制器 所有方法生效
                //在全局添加授权过滤器
                //<过滤器类名>
                x.Filters.Add<Filter.AuthorizeFilter>();
                x.Filters.Add<GlobalActionFilter>();
                //添加异常信息过滤器
                x.Filters.Add<GlobalExceptionFilter>();
                x.Filters.Add<ResourceFilter>();
                x.Filters.Add<ResultFilter>();
                //添加方法限流器
                x.Filters.Add<RateLimitActionFilter>();
            });

            //引入数据库上下文
            //1、定义对象注入
            services.AddDbContext<Store_2022Context>();

            //添加内存缓存服务
            services.AddMemoryCache();

            //添加cors 跨域
            //什么叫跨域 浏览器同源政策
            //不同域名之间调用 限制 默认拒绝请求
            //gitee.com   调用 baidu.com 数据
            //二级域名
            //www.gitee.com  调用 api.gitee.com
            //前后端分离 会造成页面域名 和 API域名 经常不一样的
            //后端需要配置跨域策略
            services.AddCors(options =>
            {
                //添加core 策略
                options.AddPolicy("Policy1", //策略名
                    builder =>
                    {
                        builder.WithOrigins("*") //表示可以被所有地址跨域访问 允许跨域访问的地址，不能以正反斜杠结尾。
                            .SetIsOriginAllowedToAllowWildcardSubdomains()//设置策略里的域名允许通配符匹配，但是不包括空。
                                                                          //例：http://localhost:3001 不会被通过
                                                                          // http://xxx.localhost:3001 可以通过
                            .AllowAnyHeader()//配置请求头
                            .AllowAnyMethod();//配置允许任何 HTTP 方法访问
                    });
            });

            //添加swagger文档配置
            services.AddSwaggerGen(options =>
            {
                options.CustomSchemaIds(x => x.FullName);
                options.SwaggerDoc("v1", new OpenApiInfo { Title = "API测试Demo", Version = "v1" });
                // 获取xml文件名
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                // 获取xml文件路径
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                // 添加控制器层注释，true表示显示控制器注释
                options.IncludeXmlComments(xmlPath, true);
                //添加授权的入口
                options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
                {
                    Description = "在下框中输入请求头中需要添加Jwt授权Token：Bearer Token",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    BearerFormat = "JWT",
                    Scheme = "Bearer"
                });
                options.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme{
                            Reference = new OpenApiReference {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"}
                        },new string[] { }
                    }
                });
            });

            //自定义服务.Add                        对象的接口                对象类             服务的生命周期
            //把对象交给IOC容器去创建
            //对象是有生命周期 IOC容器也负责对象的生命周期管理
            services.Add(new ServiceDescriptor(typeof(IDataServices), typeof(DataServices), ServiceLifetime.Singleton));

            //注入单例的服务
            services.Add(new ServiceDescriptor(typeof(ISingletonServices), typeof(SingletonServices), ServiceLifetime.Singleton));
            //瞬时服务
            services.Add(new ServiceDescriptor(typeof(ITransientService), typeof(TransientService), ServiceLifetime.Transient));
            //周期
            services.Add(new ServiceDescriptor(typeof(IScopedService), typeof(ScopedService), ServiceLifetime.Scoped));


            //生命周期
            //1、单例 ServiceLifetime.Singleton 创建后会一直存在
            //   3个人连续访问 这个单例对象 第一个人访问后悔创建 并且创建后 一直会存在容器 后面两个人都是访问同一个对象

            //2、周期 ServiceLifetime.Scoped:在每一次请求时会创建服务的新实例，并在这个请求内一直共享这个实例
            //A 、B 同时发起各一个请求 A获取这个对象 和 B获取的对象不一样
            //A在这个请求  获取了 两次这个对象 这个是两次获取对象是一样的

            //3、瞬时 ServiceLifetime.Transient:每次服务被请求时，总会创建新实例
            //A在这个请求时 每次获取对象的时候 都是一个新的实例

            //添加日志服务
            services.AddLogging(logBuilder => {
                //清楚原来自带日志组件
                logBuilder.ClearProviders();
                logBuilder.AddNLog();
            });

            //注入授权认证服务

            //读取 配置信息
            services.Configure<JWTConfig>(Configuration.GetSection("JWTConfig"));
            //获取配置信息 对象
            var tokenConfigs = Configuration.GetSection("JWTConfig").Get<JWTConfig>();

            //添加授权认证配置到 服务
            //Authentication
            services.AddAuthentication(x =>
            {
                //指定默认的授权方式
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(x => {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(tokenConfigs.Secret)),
                    ValidIssuer = tokenConfigs.Issuer,
                    ValidAudience = tokenConfigs.Audience,
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });
            //添加jwt服务
            services.Add(new ServiceDescriptor(typeof(IJWTService), typeof(JWTService), ServiceLifetime.Scoped));

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            //启用认证
            app.UseAuthentication();

            app.UseRouting();

            //使用cors，注意中间件的位置位于UseRouting与UseAuthorization之间
            app.UseCors("Policy1");//此处如果填写了app.UserCors("Policy1")，则控制器中默认使用该策略，并且不需要在控制器上添加[EnableCors("Policy1")]

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            //启用Swagger
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "coreMVC3.1");
                //c.RoutePrefix = string.Empty;
                c.RoutePrefix = "API";     //如果是为空 访问路径就为 根域名/index.html,注意localhost:端口号/swagger是访问不到的
                                           //路径配置，设置为空，表示直接在根域名（localhost:8001）访问该文件
                                           // c.RoutePrefix = "swagger"; // 如果你想换一个路径，直接写名字即可，比如直接写c.RoutePrefix = "swagger"; 则访问路径为 根域名/swagger/index.html
            });

        }
    }
}