﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WebAPI_Demo.Models.Database
{
    public partial class Category
    {
        public int Id { get; set; }
        public string CateName { get; set; }
    }
}
