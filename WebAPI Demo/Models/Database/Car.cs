﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WebAPI_Demo.Models.Database
{
    public partial class Car
    {
        public int Id { get; set; }
        public int GoodId { get; set; }
        public int Uid { get; set; }
        public int Count { get; set; }
        public DateTime CreateTime { get; set; }
    }
}
