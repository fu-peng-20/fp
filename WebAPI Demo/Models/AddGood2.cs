﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI_Demo.Models
{
    /// <summary>
    /// 请求类
    /// </summary>
    public class AddGood2
    {
        //写在指定参数的上方
        //规定字段必填
        [Required(ErrorMessage = "产品名称不为空")]
        [StringLength(50,ErrorMessage ="产品名称的长度不能大于50")]
        public string Name { get; set; }

        public string Cover { get; set; }
        public int Stock { get; set; }
        /// <summary>
        ///验证数值范围
        /// </summary>
        [Range(0, 999.99)]
        public decimal Price { get; set; }
        public int CateId { get; set; }
    }
}
