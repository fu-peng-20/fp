﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI_Demo.Models
{
    public class UpdateCategory
    {
        /// <summary>
        /// 主键
        /// </summary>
        public int Id { get; set; }

        public string CateName { get; set; }
    }
}
