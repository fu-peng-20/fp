﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI_Demo.Models
{
    public class GoodModel
    {
        /// <summary>
        /// 商品
        /// </summary>
        public int Id { get; set; }
        public string Name { get; set; }
        public int CateId { get; set; }
        public string Cover { get; set; }
        public decimal Price { get; set; }
        public int Stock { get; set; }

        /// <summary>
        /// 分类
        /// </summary>
        public string CategoryName
        {
            get; set;
        }
    }
}