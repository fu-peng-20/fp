﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI_Demo.Models
{
    /// <summary>
    /// 更新接口的请求类
    /// </summary>
    public class UpdateGood
    {
        /// <summary>
        /// 更新条件
        /// </summary>
        public int Id { get; set; }

        public string Name { get; set; }
        public string Cover { get; set; }
        public decimal Price { get; set; }
        public int CateId { get; set; }
        public int Stock { get; set; }
    }
}
