﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI_Demo.Models
{
    /// <summary>
    /// 自定义请求类 新增产品
    /// </summary>
    public class AddGoodRequest
    {

        //验证规则 写在属性上方

        //是否必填 ErrorMessage指定如果验证不通过  错误提示
        [Required(ErrorMessage = "产品名称不为空")]
        //字符串长度限制StringLength
        [StringLength(50, ErrorMessage = "字符串不能大于50")]
        public string Name { get; set; }

        //必填
        [Required]
        public int CateId { get; set; }
        public string Cover { get; set; }
        //规定一个数值范围 错误提示
        [Range(0, 999.99, ErrorMessage = "价格必须输入0-999.99")]
        public decimal Price { get; set; }
        public int Stock { get; set; }
    }
}