﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI_Demo.Models
{
    /// <summary>
    /// 查询商品列表 请求类
    /// </summary>
    public class GetGoodList1Request
    {
        /// <summary>
        /// 查询条件分类 Id
        /// </summary>
        public int CateId { get; set; }
        /// <summary>
        /// 查询关键字
        /// </summary>
        public string Keyword { get; set; }
    }
}
