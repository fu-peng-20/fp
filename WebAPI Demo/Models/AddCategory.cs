﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI_Demo.Models
{
    /// <summary>
    /// 新增分类  请求类
    /// </summary>
    public class AddCategory
    {
        //参数
        /// <summary>
        /// 分类名称
        /// </summary>
        public string CateName { get; set; }
    }
}
