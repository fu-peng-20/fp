﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI_Demo.Models
{
    /// <summary>
    /// 请求类 更新商品
    /// </summary>
    public class UpdateGood2
    {
        /// <summary>
        /// 商品Id 查询条件 
        /// </summary>
        public int Id { get; set; }

        //需要更新的字段
        public string Name { get; set; }
        public int CateId { get; set; }
        public string Cover { get; set; }
        public decimal Price { get; set; }
        public int Stock { get; set; }
    }
}