﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI_Demo.Models
{
    public class ClassA
    {
        public ClassA()
        {
            //b实例化 穿了一个参数
            var b = new ClassB(1, 2);
            b.GetData();

            //实际企业开发过程中ClassB 可能被1W此调用  耦合 高内聚 低耦合
            //依赖注入
        }
    }
}