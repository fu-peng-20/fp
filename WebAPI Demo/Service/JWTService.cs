﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using WebAPI_Demo.Models;

namespace WebAPI_Demo.Services
{
    public class JWTService : IJWTService
    {
        /// <summary>
        /// 获取jwt配置服务
        /// </summary>
        private readonly JWTConfig _jwtConfig;
        /// <summary>
        /// 构造注入
        /// </summary>
        /// <param name="jwtConfig"></param>
        public JWTService(IOptions<JWTConfig> jwtConfig)
        {
            this._jwtConfig = jwtConfig.Value;
        }
        /// <summary>
        /// 创建TOKEN方法
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public string CreateToken(string name, int userId)
        {
            //打包 登录信息载荷
            //把有需要的信息写到Token
            var claims = new[] {
                new Claim(ClaimTypes.NameIdentifier, userId.ToString()),
                new Claim(ClaimTypes.Name, name),
                new Claim("QQ",""),
            };

            //创建密钥
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtConfig.Secret));

            //密钥加密
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            //token配置
            var jwtToken = new JwtSecurityToken(_jwtConfig.Issuer,
                _jwtConfig.Audience,
                claims,
                expires: DateTime.Now.AddMinutes(_jwtConfig.AccessExpiration),
                signingCredentials: credentials);

            //获取token字符串
            var token = new JwtSecurityTokenHandler().WriteToken(jwtToken);
            return token;
        }
    }
}