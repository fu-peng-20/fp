﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI_Demo.Services
{
    public interface IJWTService
    {
        /// <summary>
        /// 创建Token
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        string CreateToken(string name, int userId);
    }
}