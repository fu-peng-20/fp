﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI_Demo.Service
{
    /// <summary>
    /// 接口  /  API 接口
    /// </summary>
    public interface IDataServices
    {
        //接口是类的抽象
        /// <summary>
        /// 获取数据
        /// </summary>
        /// <returns></returns>
        List<int> GetData(); //抽象方法 接口只需要管方法的参数 方法的名字  方法的返回值 不需要管实现
    }
}
