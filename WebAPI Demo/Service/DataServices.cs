﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI_Demo.Service
{
    /// <summary>
    /// 
    /// </summary>
    //                     类:接口
    public class DataServices: IDataServices
    {
        //类是接口的实现
        /// <summary>
        /// 获取数据
        /// </summary>
        /// <returns></returns>
        public List<int> GetData()
        {
            //方法的实现 做方法的事情
            return new List<int>();
        }
    }
}
