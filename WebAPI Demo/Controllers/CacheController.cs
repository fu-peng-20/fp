﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI_Demo.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CacheController : ControllerBase
    {
        private readonly IMemoryCache cache;
        /// <summary>
        /// 构造注入
        /// </summary>
        /// <param name="cache"></param>
        public CacheController(IMemoryCache cache)
        {
            this.cache = cache;
        }

        // KEY Value

        /// <summary>
        /// 判断缓存是否存在
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<string> TryGetValueCache()
        {
            //cache.TryGetValue("KEY键名")
            //out 关键字 引用传递
            //变量传递到方法里 在方法里修改 该变量不会进行修改 out ref _指把变量放弃掉了
            object obj;
            //TryGetValue
            //判断有没有ke'y的值存在 如果不存在方法缓存false
            //如果存在方法返回true 同时把值value传递到obj
            if (cache.TryGetValue("UserName", out obj))
            {
                //obj缓存值的内容
                return "该缓存存在";
            }
            else
            {
                return "该缓存不存在";
            }
        }
        [HttpGet]
        public int B()
        {
            int a = 5; //B执行域内存定义a
            //值传递
            //把a的值（是一个值）传进去A方法 
            A(a);
            //a==? 1??? a==5

            //引用传递
            //out使值传递 变成 引用传递
            //引用传递 传递时a的指针 内存里的地址
            C(out a);

            //a==??
            return a;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public void A(int a) //a=5 在A方法的执行域 定义了一个a变量
        {
            //赋值1
            a = 1;
        }

        [NonAction]
        public void C(out int a)
        {
            a = 2;
        }

        /// <summary>
        /// 读写缓存
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<string> GetCache()
        {
            // 写入缓存
            //cache.Set("C6","1")
            cache.Set("UserName", "admin");
            cache.Set("Password", "12345");


            // 读取缓存
            //cache.Get<string>("C6")
            //cache.Get<缓存的数值类型>("key")
            string userName = cache.Get<string>("UserName");
            string password = cache.Get<string>("Password");

            // 返回
            return userName + "\n" + password;
        }
    }
}