﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using WebAPI_Demo.Models;
using WebAPI_Demo.Models.Database;
using WebAPI_Demo.Services;

namespace WebAPI_Demo.Controllers
{
    /// <summary>
    /// 是
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        //注入jwt服务
        private readonly IJWTService _jwtService;
        private readonly Store_2022Context _db;
        /// <summary>
        /// 构造注入
        /// </summary>
        /// <param name="jwtService"></param>
        public AuthenticationController(IJWTService jwtService, Store_2022Context db)
        {
            _jwtService = jwtService;
            _db = db;
        }

        /// <summary>
        /// 创建token  
        /// 授权 找保安拿钥匙
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public string CreateToken(string name)
        {
            //调用jwt服务里的创建token方法
            return _jwtService.CreateToken(name, 0);
        }

        /// <summary>
        /// 认证 门锁对钥匙进行认证
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        public string UserInfo()
        {
            //门锁 告诉我 我是谁 
            //获取用户信息 登录用户的信息 认证信息
            return Response.HttpContext.User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Name).Value;
        }

        //注册 添加一个新用户 
        /// <summary>
        /// 注册
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public string AddUser(AddUserRequest request)
        {
            //注册逻辑

            //检查用户名是否存在
            //查询有没有这个用户名
            if (_db.Users.Any(x => x.UserName == request.UserName))
            {
                return "用户名已存在";
            }

            //添加一个新的用户
            //定义一个用户对象
            var user = new User()
            {
                UserName = request.UserName,
                Password = request.Password,
                CraeteTime = DateTime.Now,
                Photo = string.Empty,
                Desc = string.Empty
            };
            _db.Users.Add(user);
            var row = _db.SaveChanges();
            if (row > 0)
                return "注册成功";
            return "注册失败";
        }

        [HttpPost]
        public string Login(LoginRequest request)
        {
            //怎么用参数

            //登录 - 校验账号密码是否正确  
            //查询这个用户名 下的用户
            var user = _db.Users.FirstOrDefault(x => x.UserName == request.UserName);
            if (user == null)
                return "账号不存在";
            //校验密码
            if (user.Password == request.Password)
            {
                //成功 
                //创建钥匙 给用户
                return _jwtService.CreateToken(user.UserName, user.UserId);
            }
            return "密码不正确";


        }

        /// <summary>
        /// 获取当前时间 
        /// </summary>
        /// <returns></returns>   
       // [ResponseCache(Duration = 60)]//适合静态资源
        [HttpGet]
        public DateTime Now()
        {
            return DateTime.Now;
        }

        //服务端缓存
        //服务端 每个用户获取同一个资源 都是返回同一个
        //A用户 获取一个用户信息接口 头像 姓名 A用户
        //B用户 获取一个用户信息接口 自己头像 姓名 获取缓存下的数据 A用户信息 。
        //不能使用服务端缓存 在有登录信息的接口
    }
}