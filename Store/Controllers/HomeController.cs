﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Store.Models;
using Store.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Store.Controllers
{
    //在url上 不区分大小写的

    /// <summary>
    /// Home控制器
    /// </summary>
    [Route("/[action].html")]
    [Route("[controller]/[action]")]
    public class HomeController : Controller
    {
        //定义上下文变量
        private readonly Store_2022Context _db;
        //构造注入 数据库上下文
        public HomeController(Store_2022Context db)
        {
            _db = db;
        }
        //使用数据库查询

        //1、查询数据 条件查询 var list =_db.表名.Where().Tolist();

        //2、把list传递到页面上  ViewData ViewBag ViewModel

        //3、把传递过来的数据 渲染到页面

        /// <summary>
        /// 控制器方法 
        /// 添加路由实现 url /index.html  /Home/Index
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            //找试图 规则/Views/Home(控制器名字)/Index.cshtml（方法的名字）
            return View();
        }
        /// <summary>
        /// 商品列表方法
        /// </summary>
        /// <returns></returns>
        public IActionResult Market(int cateId)
        {
            //1、查询 分类数据
            //           数据库上下文 所有分类结果
            var cates = _db.Categories.ToList();
            //2、传递 分类数据
            //      参数的名字    参数的值
            ViewData["Cates"] = cates;
            if(cateId==0)
            {
                //取以第一个分类Id
                cateId = cates.FirstOrDefault().Id;
            }    
            //1、查询 分类数据
            var goods = _db.Goods.Where(x=>x.CateId== cateId).ToList();
            //2、传递 分类数据
            //      参数的名字    参数的值
            ViewData["Goods"] = goods;
            ViewData["CateId"] = cateId;

            return View();
        }
        /// <summary>
        /// 商品详情
        /// </summary>
        /// <returns></returns>
        public IActionResult Detail(int id)
        {
            //数据查询

            //一个商品数据
            //上下文.表名.查询一个商品(根据条件) 哪个商品
            var good = _db.Goods.FirstOrDefault(x => x.Id == id);
            //2、传递 数据
            //      参数的名字    参数的值
            ViewData["Good"] = good;
            return View();
        }
        /// <summary>
        /// 关于我们
        /// </summary>
        /// <returns></returns>
        public IActionResult About()
        {
            return View();
        }
        /// <summary>
        /// 个人中心
        /// </summary>
        /// <returns></returns>
        /// 指明方法需要登陆
        [Authorize]
        public IActionResult Userinfo()
        {
            //获取 登录信息中的 用户Id
            //HttpContext.User.Claims 登录信息的列表
            var data = HttpContext.User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier).Value;
            // HttpContext.User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Name).Value;
            // HttpContext.User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Name).Value;
            // HttpContext.User.Claims.FirstOrDefault(x => x.Type == "QQ").Value;

            //获取授权中的用户Id
            var userId = Convert.ToInt32(data);
            //在数据库查询用户数据
            var user = _db.Users.FirstOrDefault(x => x.UserId == userId);
            //传递到视图上
            ViewData["User"] = user;

            //查询用户购物车数据
            //用户的购物车数据
            var list = _db.Cars.Where(x => x.Uid == userId).Join(_db.Goods, x => x.GoodId, y => y.Id,
                (x, y) => new CarModel
            {
                Name = y.Name,
                Cover = y.Cover,
                Price = y.Price,
                Id = y.Id,
                Count = x.Count,
            }).ToList();
            //传递到视图上
            ViewData["Cars"] = list;

            return View();
        }


        //    注册/登录

        // 1、数据库设计
        //    用户表
        //    主键 账号 密码 头像 个人说明

        // 2、前端页面设计

        // 3、控制器方法
        [Route("/Register.html")]
        public IActionResult Register()
        {
            return View();
        }

        /// <summary>
        /// 注册账号
        /// </summary>
        /// <returns></returns>
        //指定下面方法的请求方式（GET POST PUT 。。。。。）
        [HttpPost]
        public IActionResult RegisterAccount(RegisterAccountRequest request)
        {
            //判断用户名是否存在 如果存在我就返回 不让注册
            var result = _db.Users.Any(x => x.UserName == request.UserName);
            if (result)
                return Content("账号已存在");

            //添加数据 
            var data = new User
            {
                UserName = request.UserName,
                Password = request.Password,
                CraeteTime = DateTime.Now,
                Photo = String.Empty,
            };
            //用户表数据添加
            //上下文.表名.操作方法
            _db.Users.Add(data);
            var row = _db.SaveChanges();
            if (row > 0)
                return Content("注册成功");
            return Content("注册失败");
        }

        //登录页面视图

        [Route("/Login.html")]
        public IActionResult Login()
        {
            return View();
        }

        //登录

        //1、接收前端传递过来参数  账号、密码
        [HttpPost]
        public IActionResult LoginAccount(LoginAccountRequest request)
        {
            //2、验证账号密码是否正确
            //1.用账号去查询数据库 有无这个数据
            //select *  from [User] where UserName='123'
            //查询用户 查询一个用户 条件 用户名= 前端传过来用户名
            var user = _db.Users.FirstOrDefault(x => x.UserName == request.UserName);
            //  如果存在 有这个账号
            if(user!=null)
            {
                // 查看结果的密码是否 等于 前端传过来密码
                if(user.Password==request.Password)
                {
                    //1、对登录账号进行授权认证 写入登录状态

                    //存在的话 走登录流程
                    //需要保存的登录信息 列表
                    var claims = new List<Claim>
                    {
                        //                 Key                Value
                        //Id          登录信息的类型（名字）
                        new Claim(ClaimTypes.NameIdentifier,user.UserId.ToString()),
                        //账号
                        new Claim(ClaimTypes.Name, user.UserName),
                        //头像
                        new Claim(ClaimTypes.Thumbprint, user.Photo),
                        //自定义类型 QQ
                        //new Claim("QQ","QQ的值")
                    };
                    //ViewData["名字"]="参数的值"
                    //ViewData["名字"]
                    //把上面登录信息写入登录状态（http上下文）
                    HttpContext.SignInAsync(new ClaimsPrincipal(new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme)));

                    //如果相等 密码正确 
                    return Content("登录成功");
                }
                else
                {
                    // 返回密码不正确
                    return Content("密码不正确");
                }
            }
            //用户不存在
            else
            {
                return Content("用户不存在");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult LoginOut()
        {
            //执行退出登录操作
            HttpContext.SignOutAsync();
            //跳转（重定向）到首页
            //return RedirectToAction("Index");
            return Redirect("/Index.html");
        }
        /// <summary>
        /// 加入购物车
        /// </summary>
        /// <returns></returns>
        /// 1、接受前端传过来商品Id
        [Authorize]
        public IActionResult AddCar(AddCarRequest request)
        {
            var data = HttpContext.User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier).Value;

            //获取授权中的用户Id
            var uId = Convert.ToInt32(data);
            //2、加入购物车需求
            //  如果购物车有这个商品记录 购物车的记录 数量+1 


            //                                条件   商品Id=前端商品Id并且 用户Id==登录用户Id                    
            var record = _db.Cars.FirstOrDefault(x=>x.GoodId==request.GoodId&&x.Uid==uId);
            if(record!=null)//存在
            {
                //自增1
                record.Count+=1;
                _db.Cars.Update(record);
            }
            else
            {
            //  否则在购物车表创建一个条记录
                record = new Car
                {
                    GoodId = request.GoodId,
                    Uid = uId,
                    CreateTime = DateTime.Now,
                    Count = 1
                };
                _db.Cars.Add(record);
            }
            var row = _db.SaveChanges();
            if (row > 0)
                return Content("加入成功");
            return Content("加入失败");
        }
    }
}
