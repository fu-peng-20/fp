﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Store.Models
{
    public class RegisterAccountRequest
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
