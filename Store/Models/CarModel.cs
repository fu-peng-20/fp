﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Store.Models
{
    public class CarModel
    {
        public string Name { get; set; }
        public string Cover { get; set; }
        public decimal Price { get; set; }
        public int  Id { get; set; }
        public int Count { get; set; }
    }
}
