﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Store.Models.Database
{
    public partial class Category
    {
        public int Id { get; set; }
        public string CateName { get; set; }
    }
}
