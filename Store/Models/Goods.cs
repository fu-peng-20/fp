﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Store.Models
{
    /// <summary>
    /// 商品类
    /// </summary>
    public class Goods
    {
        public int GoodId { get; set; }
        public string GoodName { get; set; }
        public decimal Price { get; set; }
        public string Img { get; set; }

        //...
    }
}
