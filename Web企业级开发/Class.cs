﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web企业级开发
{
    public class ClassB
    {
        /// <summary>
        /// 无参构造方法
        /// </summary>
        /// 访问修饰符  类的名字
        public ClassB()
        {
            //实例化类的执行
            var a = 1;
        }
        /// <summary>
        /// 有参构造方法
        /// </summary>
        /// <param name="name"></param>
        public ClassB (string name)
        {
            var b = 1;

        }
        /// <summary>
        /// 有参构造方法
        /// </summary>
        /// <param name="name"></param>
        public ClassB(int age)
        {
            var b = 1;

        }
        //private double salary;//薪水
        //public double Salary
        //{
        //    get { return salary; }
        //    set { salary == value; }
        //}

        //属性 简化
        public  double Salary { get; set; }
        public  string Name { get; set; }
        /// <summary>
        /// 说话
        /// </summary>
        public  void Say()
        {

        }
    }
}
