﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web企业级开发
{
    //访问修饰符 class 类名
    public class Student
    {
        //4.1 该类有以下属性，学生学号，学生姓名，学生班级，学生的出生年月，学生的籍贯
        //prop + TAB键
        //访问修饰符 类型 属性的名字
        //学生学号 命名规则 属性（大驼峰）
        public int StuNo { get; set; }
        /// <summary>
        /// 学生姓名
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 班级
        /// </summary>
        public string ClassName { get; set; }
        /// <summary>
        /// 生日
        /// </summary>
        public DateTime Birthday { get; set; }
        /// <summary>
        /// 籍贯
        /// </summary>
        public string Address { get; set; }

        //4.2 该类有两个构造函数，第一个是无参构造函数，第二个是构造函数需要传入学生学号，学生姓名。
        /// <summary>
        /// 无参构造函数
        /// </summary>
        ///访问修饰符 类名
        public Student()
        {

        }
        /// <summary>
        /// 有参构造函数
        /// </summary>
        /// <param name="stuNo">学号</param>
        /// <param name="name">姓名</param>
        public Student(int stuNo,string name)
        {
            //初始化属性 赋值
            StuNo = stuNo;
            Name = name;
        }
        /// <summary>
        /// 有参构造函数 5个参数
        /// </summary>
        /// <param name="stuNo">学号</param>
        /// <param name="name">姓名</param>
        public Student(int stuNo, string name,string className,DateTime birthday,string address)
        {
            //初始化属性 赋值
            StuNo = stuNo;
            Name = name;
            ClassName = className;
            Birthday = birthday;
            Address = address;
        }
        //4.3 该类有一个方法Say,方法返回学生的自我介绍信息
        //方法组成 访问修饰符 类型 方法名()
        public string Say()
        {
            //方法返回学生的自我介绍信息
            return $"大家好，我是来自{Address}的{Name}，我的学号是{StuNo}，我今年{DateTime.Now.Year-Birthday.Year}岁。";
        }
    }
}
