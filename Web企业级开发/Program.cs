using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web企业级开发
{
    //
    public class Program
    {

        /// <summary>
        /// 应用程序的入口 
        /// </summary>
        /// <param nam
        /// <summary>e="args"></param>
        public static void Main(string[] args)
        {
            //创建了站点创造器     执行创建   运行站点
            CreateHostBuilder(args).Build().Run();
        }

        /// 创建了站点创造器
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            //站点类 创建默认的创建器
            Host.CreateDefaultBuilder(args)
                //进行默认配置
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    //使用了   Startup   配置文件
                    webBuilder.UseStartup<Startup>();
                });
    }
}
