﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web企业级开发
{
    /// <summary>
    /// 猫类
    /// </summary>
    public class Cat
    {
        /// <summary>
        /// 无参构造方法
        /// </summary>
        public Cat()
        {
            //
            var a = 1;
        }
        /// <summary>
        /// 有参构造函数
        /// </summary>
        /// <param name="name"></param>
        public Cat(string name)
        {
            Name = name;
        }
        /// <summary>
        /// 有参构造函数
        /// </summary>
        /// <param name="name"></param>
        public Cat(string name,DateTime date)
        {
            Name = name;
            Date = date;
        }

        /// <summary>
        /// 出生日期
        /// </summary>
        public DateTime Date { get; set; }
        /// <summary>
        /// 品种
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// 名字
        /// </summary>
        public  string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public  void Say()
        {

        }
    }
}
